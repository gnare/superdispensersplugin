package com.voxelbuster.superdispensers;

import com.voxelbuster.superdispensers.event.DispenseEventHandler;
import lombok.Getter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

@Getter
@SuppressWarnings("unused")
public final class SuperDispensersPlugin extends JavaPlugin {
    @Getter
    private static SuperDispensersPlugin instance;

    private final Logger log = this.getLogger();

    private FileConfiguration config = super.getConfig();

    public SuperDispensersPlugin() {
        instance = this;
    }

    @Override
    public void onEnable() {
        super.saveDefaultConfig();

        log.info("Registering event handlers for Super Dispensers.");

        config = super.getConfig();

        this.getServer().getPluginManager().registerEvents(new DispenseEventHandler(), this);
    }

    @Override
    public void onDisable() {
        log.info("Super Dispensers disabled.");
    }

}
