package com.voxelbuster.superdispensers.event;

import com.voxelbuster.superdispensers.SuperDispensersPlugin;
import com.voxelbuster.superdispensers.util.GameCollections;
import org.bukkit.Axis;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.SoundGroup;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.Orientable;
import org.bukkit.block.data.type.Sapling;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Breedable;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Vehicle;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static org.bukkit.Material.AIR;
import static org.bukkit.Material.DIAMOND_PICKAXE;
import static org.bukkit.Material.DIRT_PATH;
import static org.bukkit.Material.FARMLAND;
import static org.bukkit.Material.GOLDEN_PICKAXE;
import static org.bukkit.Material.IRON_PICKAXE;
import static org.bukkit.Material.NETHERITE_PICKAXE;
import static org.bukkit.Material.STONE_PICKAXE;
import static org.bukkit.Material.WOODEN_PICKAXE;

public abstract class DispenseEventHelpers {
    // TODO test block breaking

    /**
     * Breaks a block relative to the dispenser. If the block is broken, it will drop the block as an item.
     * @param dispenserState the dispenser block state
     * @param stack the tool being used to break the block
     * @param dispenserBlock the dispenser block
     * @param b the block to break
     */
    public void breakBlockWithDispenser(
        org.bukkit.block.Dispenser dispenserState, ItemStack stack,
        Block dispenserBlock, Block b
    ) {
        if (hasSilkTouch(stack)) {
            dispenserBlock.getWorld().playSound(
                b.getLocation(),
                b.getBlockData().getSoundGroup().getBreakSound(),
                1f,
                1f
            );
            dispenserBlock.getWorld().dropItemNaturally(
                b.getLocation(),
                new ItemStack(b.getType(), 1)
            );
            ItemStack particleType = new ItemStack(b.getType());
            b.setType(AIR);
            Bukkit.getScheduler().scheduleSyncDelayedTask(
                SuperDispensersPlugin.getInstance(),
                () -> dispenserBlock.getWorld().spawnParticle(
                    Particle.ITEM_CRACK,
                    b.getLocation(),
                    60,
                    0.5,
                    0.5,
                    0.5,
                    particleType
                ),
                1L
            );
        } else {
            dispenserBlock.getWorld().playSound(
                b.getLocation(),
                b.getBlockData().getSoundGroup().getBreakSound(),
                1f,
                1f
            );
            ItemStack particleType = new ItemStack(b.getType());
            b.breakNaturally(stack);
            Bukkit.getScheduler().scheduleSyncDelayedTask(
                SuperDispensersPlugin.getInstance(),
                () -> dispenserBlock.getWorld().spawnParticle(
                    Particle.ITEM_CRACK,
                    b.getLocation(),
                    60,
                    0.5,
                    0.5,
                    0.5,
                    particleType
                ),
                1L
            );
        }
        damageItemStack(stack, dispenserState.getInventory());
    }

    /**
     * Attempts to convert dirt to a path relative to the dispenser. This only works if the dirt is directly in
     * front of the dispenser face, or one block below it. Uses {@link Tag#DIRT} to determine
     * which dirt blocks are valid.
     *
     * @param source
     *     the dispenser block
     * @param face
     *     the direction the dispenser is facing
     *
     * @return true if the dirt was converted to a path, false otherwise.
     */
    public boolean tryPathDirt(Block source, BlockFace face) {
        Location blockLoc = source.getLocation().add(face.getDirection());
        if (blockLoc.getWorld() == null) {
            return false;
        }

        if (Tag.DIRT.getValues().contains(blockLoc.getBlock().getType())) {
            blockLoc.getWorld().setBlockData(
                blockLoc,
                DIRT_PATH.createBlockData()
            );
            return true;
        }
        if (Tag.DIRT.getValues().contains(blockLoc.add(BlockFace.DOWN.getDirection()).getBlock().getType())) {
            blockLoc.getWorld().setBlockData(
                blockLoc,
                DIRT_PATH.createBlockData()
            );
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets all entities of a specific type within a 1x1x1 cube centered on the block face of the dispenser.
     *
     * @param block the dispenser block
     * @param facing the direction the dispenser is facing
     * @param type the entity type to search for
     * @return a stream of entities of the specified type
     */
    public Stream<Entity> getNearbyEntities(Block block, BlockFace facing, EntityType type) {
        return block.getWorld().getNearbyEntities(block.getRelative(facing).getLocation(), 0.5, 1, 0.5)
                .stream()
                .filter(e -> e.getType().equals(type));
    }

    /**
     * Gets all breedable entities within a 1x1x1 cube centered on the block face of the dispenser.
     *
     * @param block the dispenser block
     * @param facing the direction the dispenser is facing
     * @param mat the material of the item being dispensed
     * @return a stream of breedable entities
     */
    public Stream<Breedable> getBreedableCreatures(Block block, BlockFace facing, Material mat) {
        return block.getWorld().getNearbyEntities(block.getRelative(facing).getLocation(), 0.5, 1, 0.5)
            .stream()
            .filter(Breedable.class::isInstance)
            .map(Breedable.class::cast)
            .filter(Breedable::canBreed)
            .filter(b -> {
                String cUuid = b.getUniqueId().toString();

                b.getPersistentDataContainer();

                String name = "";

                if (b.getCustomName() != null) {
                    name = b.getCustomName();
                }

                // ATTN: this may change between MC versions
                Bukkit.dispatchCommand(
                    Bukkit.getConsoleSender(),
                    "execute if data entity " +
                        cUuid +
                        " {InLove:0} run data merge entity " +
                        cUuid +
                        " {CustomName:_breedTrue_}"
                );

                boolean result = b.getCustomName() != null && b.getCustomName().startsWith("_breedTrue_");
                if (result) {
                    b.setCustomName(name);
                }
                return result;
            })
            .filter(b -> {
                Set<EntityType> types = GameCollections.getBreedingItemsMap().get(mat);
                if (types == null || types.isEmpty()) {
                    return false;
                } else {
                    return types.contains(b.getType());
                }
            });
    }

    /**
     * Attempts to till dirt relative to the dispenser. This only works if the dirt is directly in
     * front of the dispenser face, or one block below it. Uses {@link Tag#DIRT} to determine
     * which dirt blocks are valid.
     *
     * @param source
     *     the dispenser block
     * @param face
     *     the direction the dispenser is facing
     *
     * @return true if the dirt was tilled, false otherwise.
     */
    public boolean tryTillDirt(Block source, BlockFace face) {
        Location blockLoc = source.getLocation().add(face.getDirection());
        if (blockLoc.getWorld() == null) {
            return false;
        }

        if (Tag.DIRT.getValues().contains(blockLoc.getBlock().getType())) {
            blockLoc.getWorld().setBlockData(
                blockLoc,
                FARMLAND.createBlockData()
            );
            return true;
        }
        if (Tag.DIRT.getValues().contains(blockLoc.add(BlockFace.DOWN.getDirection()).getBlock().getType())) {
            blockLoc.getWorld().setBlockData(
                blockLoc,
                FARMLAND.createBlockData()
            );
            return true;
        } else {
            return false;
        }
    }

    /**
     * Attempts to plant a sapling on dirt relative to the dispenser. This only works if the dirt is directly in
     * front of the dispenser face, or one block below it. Uses {@link Tag#DIRT} to determine
     * which dirt blocks are valid.
     *
     * @param source
     *     the dispenser block
     * @param face
     *     the direction the dispenser is facing
     * @param material
     *     the dispensed item type
     *
     * @return true if the sapling was planted, false otherwise.
     */
    public boolean attemptPlantTree(Block source, BlockFace face, Material material) {
        if (!(material.createBlockData() instanceof Sapling)) {
            return false;
        }

        Location blockLoc = source.getLocation().add(face.getDirection());
        if (blockLoc.getWorld() == null) {
            return false;
        }

        if (Tag.DIRT.getValues().contains(blockLoc.getBlock().getType())) {
            blockLoc.getWorld().setBlockData(
                blockLoc.add(BlockFace.UP.getDirection()),
                material.createBlockData()
            );
            return true;
        }
        if (Tag.DIRT.getValues().contains(blockLoc.add(BlockFace.DOWN.getDirection()).getBlock().getType())) {
            blockLoc.getWorld().setBlockData(
                blockLoc.add(BlockFace.UP.getDirection()),
                material.createBlockData()
            );
            return true;
        } else {
            return false;
        }
    }

    /**
     * Attempts to plant seeds on farmland relative to the dispenser. This only works if the farmland is directly in
     * front of the dispenser face, or one block below it. Uses {@link GameCollections#getCropsMap()} to determine
     * which seeds are valid.
     *
     * @param source
     *     the dispenser block
     * @param face
     *     the direction the dispenser is facing
     * @param material
     *     the dispensed item type
     * @param materialPlantedOn
     *     the material the seeds are planted on
     *
     * @return true if the seeds were planted, false otherwise.
     */
    public boolean attemptPlantSeeds(Block source, BlockFace face, Material material, Material materialPlantedOn) {
        Location blockLoc = source.getLocation().add(face.getDirection());
        if (GameCollections.getCropsMap().containsKey(material)) {
            if (blockLoc.getWorld() == null) {
                return false;
            }

            if (blockLoc.getBlock().getType() == materialPlantedOn) {
                blockLoc.getWorld().setBlockData(
                    blockLoc.add(BlockFace.UP.getDirection()),
                    GameCollections.getCropsMap().get(material).createBlockData()
                );
                return true;
            }
            if (blockLoc.add(BlockFace.DOWN.getDirection()).getBlock().getType() == materialPlantedOn) {
                blockLoc.getWorld().setBlockData(
                    blockLoc.add(BlockFace.UP.getDirection()),
                    GameCollections.getCropsMap().get(material).createBlockData()
                );
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Attempts to plant seeds on farmland relative to the dispenser. This only works if the farmland is directly in
     * front of the dispenser face, or one block below it. Uses {@link GameCollections#getCropsMap()} to determine
     * which seeds are valid.
     *
     * @param source the dispenser block
     * @param face the direction the dispenser is facing
     * @param material the dispensed item type
     *
     * @return true if the seeds were planted, false otherwise.
     */
    public boolean attemptPlantSeeds(Block source, BlockFace face, Material material) {
        return attemptPlantSeeds(source, face, material, FARMLAND);
    }

    /**
     * Reduces the durability of an ItemStack by 1, if it can have durability.
     *
     * @param stack
     *     the {@link ItemStack}
     */
    public void damageItemStack(ItemStack stack, Inventory inv) {
        if (stack.getItemMeta() instanceof org.bukkit.inventory.meta.Damageable damageable) {
            // Simulate vanilla unbreaking check
            Map<Enchantment, Integer> enchants = stack.getEnchantments();
            int unbreakingLevel = enchants.getOrDefault(Enchantment.DURABILITY, 0);
            double unbreakingCalc = 1d / (unbreakingLevel + 1d); // 0 = 1.0, I = 0.5, II = 0.33..., III = 0.25
            if (Math.random() < unbreakingCalc) {
                damageable.setDamage(damageable.getDamage() + 1);
            }

            stack.setItemMeta(damageable);

            int maxDamage = stack.getType().getMaxDurability();

            if (damageable.getDamage() >= maxDamage) {
                decrementInventoryByMaterial(inv, stack);
            } else {
                Bukkit.getScheduler().scheduleSyncDelayedTask(
                    SuperDispensersPlugin.getInstance(),
                    () -> {
                        int slot = inv.first(stack.getType());
                        if (slot < 0) {
                            return;
                        }

                        inv.setItem(slot, stack);
                    },
                    1L
                );
            }
        }
    }

    /**
     * Attempts to strip the block relative to a dispenser,
     * based on {@link GameCollections#getStrippablesMap()}
     *
     * @param source
     *     the dispenser
     * @param face
     *     the direction the dispenser is facing
     *
     * @return true if the block was successfully stripped, false otherwise.
     */
    public boolean tryStripBlock(Block source, BlockFace face) {
        Location blockLoc = source.getLocation().add(face.getDirection());
        Material mat = blockLoc.getBlock().getType();

        if (GameCollections.getStrippablesMap().containsKey(mat)) {
            if (blockLoc.getBlock().getBlockData() instanceof Orientable o) {
                setBlockWithAxis(GameCollections.getStrippablesMap().get(mat), blockLoc, o.getAxis());
            } else if (blockLoc.getBlock().getBlockData() instanceof Directional d) {
                setBlockWithDirection(GameCollections.getStrippablesMap().get(mat), blockLoc, d.getFacing());
            } else {
                setBlockWithDirection(GameCollections.getStrippablesMap().get(mat), blockLoc, face);
            }

            SoundGroup soundGroup = source.getRelative(face).getBlockData().getSoundGroup();
            source.getWorld().playSound(
                source.getRelative(face).getLocation(),
                soundGroup.getBreakSound(),
                soundGroup.getVolume(),
                soundGroup.getPitch()
            );
            
            return true;
        }

        return false;
    }

    /**
     * Attempts to damage mobs/players at the specified block location. Only works if <code>stack</code>
     * is in {@link GameCollections#getWeaponDamageMap()}, otherwise this does nothing.
     *
     * @param blockLoc
     *     the location targeted by the dispenser
     * @param stack
     *     the {@link ItemStack} being dispensed
     *
     * @return true if successful, false otherwise.
     */
    public boolean tryAttackEntity(Location blockLoc, ItemStack stack) {
        if (blockLoc.getWorld() == null) {
            return false;
        }

        boolean success = false;
        Collection<Entity> entities = blockLoc.getWorld().getNearbyEntities(blockLoc, 0.5, 1, 0.5);
        for (Entity entity : entities) {
            if (
                entity instanceof Damageable damageable
                    && GameCollections.getWeaponDamageMap().containsKey(stack.getType())
            ) {
                int sharpnessLevel = stack.getEnchantmentLevel(Enchantment.DAMAGE_ALL);
                double sharpnessBonus = sharpnessLevel > 0 ? (0.5 * sharpnessLevel + 0.5) : 0d;

                double smiteBonus = 0d;
                if (GameCollections.getUndead().contains(entity.getType())) {
                    int smiteLevel = stack.getEnchantmentLevel(Enchantment.DAMAGE_UNDEAD);
                    smiteBonus = smiteLevel * 2.5;
                }

                double baneBonus = 0d;
                if (GameCollections.getArthropods().contains(entity.getType())) {
                    int baneLevel = stack.getEnchantmentLevel(Enchantment.DAMAGE_ARTHROPODS);
                    baneBonus = baneLevel * 2.5;
                    if (entity instanceof LivingEntity le) {
                        le.addPotionEffect(
                            new PotionEffect(
                                PotionEffectType.SLOW,
                                (int) Math.round(1.5 + 0.5 * baneLevel),
                                3
                            )
                        );
                    }
                }

                int fireAspectLevel = stack.getEnchantmentLevel(Enchantment.FIRE_ASPECT);
                if (fireAspectLevel > 0) {
                    entity.setFireTicks(fireAspectLevel * 80);
                }

                damageable.damage(
                    GameCollections.getWeaponDamageMap().get(stack.getType())
                        + sharpnessBonus + smiteBonus + baneBonus,
                    damageable
                );

                success = true;
                break;
            }
        }

        return success;
    }

    /**
     * Attempts to decrement the specified item from the inventory.
     *
     * @param inv
     *     the inventory
     * @param stack
     *     the {@link ItemStack} whose type to use to decrement the item in the container (amount or data
     *     need not match)
     *
     * @return true if successful, false otherwise.
     */
    public boolean decrementInventoryByMaterial(Inventory inv, ItemStack stack) {
        if (stack.getAmount() == 1) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(
                SuperDispensersPlugin.getInstance(),
                () -> {
                    int slot = inv.first(stack.getType());
                    if (slot < 0) {
                        return;
                    }

                    inv.getContents()[slot].setAmount(
                        inv.getContents()[slot].getAmount() - 1
                    );
                },
                1L
            );

            return true;
        }

        int slot = inv.first(stack.getType());
        if (slot < 0) {
            return false;
        }

        inv.getContents()[slot].setAmount(
            inv.getContents()[slot].getAmount() - 1
        );

        return true;
    }

    /**
     * Attempts to place a block relative to a dispenser. Does nothing if the location is already occupied
     * by a solid block or an entity. Also updates the block on the next tick.
     *
     * @param source
     *     the block object representing the dispenser
     * @param face
     *     the direction the dispenser is facing
     * @param material
     *     the material of the block to set
     *
     * @return true if the block could be placed, false otherwise.
     */
    public boolean attemptPlaceBlock(Block source, BlockFace face, Material material) {
        Location blockLoc = source.getLocation().add(face.getDirection());

        if (
            Tag.REPLACEABLE.getValues().contains(blockLoc.getBlock().getType())
                && hasNoEntity(blockLoc)
        ) {
            setBlockWithDirection(material, blockLoc, face);
            Bukkit.getScheduler().scheduleSyncDelayedTask(
                SuperDispensersPlugin.getInstance(),
                () -> {
                    blockLoc.getBlock().getState().update(true, true);
                    Block relativeBlock = blockLoc.getBlock().getRelative(BlockFace.NORTH);
                    relativeBlock.setBlockData(relativeBlock.getBlockData());
                },
                1L
            );
            return true;
        }

        return false;
    }

    /**
     * Sets a block with a specific axis orientation. If the block is not an instance of {@link Orientable}, this method
     * will simply set the block with the given material.
     *
     * @param material the material to set
     * @param blockLoc the location of the block
     * @param axis the axis orientation
     */
    public void setBlockWithAxis(Material material, Location blockLoc, Axis axis) {
        if (blockLoc.getWorld() == null) {
            return;
        }

        Bukkit.getScheduler().scheduleSyncDelayedTask(
            SuperDispensersPlugin.getInstance(),
            () -> {
                BlockData data = material.createBlockData();
                if (axis != null && data instanceof Orientable orientable) {
                    orientable.setAxis(axis);
                    blockLoc.getBlock().setBlockData(orientable);
                } else {
                    blockLoc.getBlock().setBlockData(data);
                }
            },
            1L
        );
    }

    /**
     * Sets a block with a specific facing direction. If the block is not an instance of {@link Directional}, this method
     * will simply set the block with the given material.
     *
     * @param material the material to set
     * @param blockLoc the location of the block
     * @param facing the facing direction
     */
    public void setBlockWithDirection(Material material, Location blockLoc, BlockFace facing) {
        if (blockLoc.getWorld() == null) {
            return;
        }

        BlockData data = material.createBlockData();
        if (facing != null && data instanceof Directional directional) {
            directional.setFacing(facing);
            blockLoc.getBlock().setBlockData(directional);
        } else {
            blockLoc.getBlock().setBlockData(data);
        }
    }

    /**
     * Checks if the targeted block location is free of entities. Any {@link Damageable}, {@link Vehicle},
     * or {@link Hanging} entities are included in this check. Dropped items are not included. This is mainly
     * intended for aiding in the automated placement of blocks. If you want to find a specific entity, use
     * {@link org.bukkit.World#getNearbyEntities(Location, double, double, double)} instead.
     *
     * @param blockLoc
     *     the location to check
     *
     * @return true if no such entities were found, false otherwise
     */
    public boolean hasNoEntity(Location blockLoc) {
        if (blockLoc.getWorld() == null) {
            return false;
        }

        Collection<Entity> entities = blockLoc.getWorld().getNearbyEntities(blockLoc, 0.5, 1, 0.5);

        if (entities.isEmpty()) {
            return true;
        } else {
            for (Entity entity : entities) {
                if (entity instanceof Damageable || entity instanceof Vehicle || entity instanceof Hanging) {
                    return false;
                }
            }
        }

        return true;
    }

    public long calcBreakTicks(Material breaking, ItemStack tool) {
        double speed = 1d;
        if (canHarvest(breaking, tool) && tool.getType().equals(NETHERITE_PICKAXE)) {
            speed = 9d;
        } else if (canHarvest(breaking, tool) && tool.getType().equals(DIAMOND_PICKAXE)) {
            speed = 8d;
        } else if (canHarvest(breaking, tool) && tool.getType().equals(IRON_PICKAXE)) {
            speed = 6d;
        } else if (canHarvest(breaking, tool) &&  tool.getType().equals(STONE_PICKAXE)) {
            speed = 4d;
        } else if (canHarvest(breaking, tool) && tool.getType().equals(GOLDEN_PICKAXE)) {
            speed = 12d;
        } else if (canHarvest(breaking, tool) && tool.getType().equals(WOODEN_PICKAXE)) {
            speed = 2d;
        }

        int efficiencyLevel = tool.getEnchantmentLevel(Enchantment.DIG_SPEED);
        if (efficiencyLevel > 0) {
            speed += Math.pow(efficiencyLevel, 2) + 1;
        }

        double damage = speed / breaking.getHardness();

        if (canHarvest(breaking, tool)) {
            damage /= 30;
        } else {
            damage /= 100;
        }

        return (long) (Math.ceil(1d / damage));
    }

    /**
     * Determines if a tool can harvest a block.
     *
     * @param breaking the block being broken
     * @param tool the tool being used
     * @return true if the tool can harvest the block, false otherwise
     */
    public boolean canHarvest(Material breaking, ItemStack tool) {
        if (Tag.NEEDS_DIAMOND_TOOL.getValues().contains(breaking) && tool.getType().equals(NETHERITE_PICKAXE)) {
            return true;
        } else if (Tag.NEEDS_DIAMOND_TOOL.getValues().contains(breaking) &&
            tool.getType().equals(DIAMOND_PICKAXE)) {
            return true;
        } else if (Tag.NEEDS_IRON_TOOL.getValues().contains(breaking) && tool.getType().equals(IRON_PICKAXE)) {
            return true;
        } else if (Tag.NEEDS_STONE_TOOL.getValues().contains(breaking) && tool.getType().equals(STONE_PICKAXE)) {
            return true;
        } else {
            return (Tag.MINEABLE_PICKAXE.getValues().contains(breaking)
                || Tag.MINEABLE_AXE.getValues().contains(breaking)
                || Tag.MINEABLE_SHOVEL.getValues().contains(breaking)
                || Tag.MINEABLE_HOE.getValues().contains(breaking))
                && !(Tag.NEEDS_DIAMOND_TOOL.getValues().contains(breaking)
                || Tag.NEEDS_IRON_TOOL.getValues().contains(breaking) && tool.getType().equals(IRON_PICKAXE)
                || Tag.NEEDS_STONE_TOOL.getValues().contains(breaking) && tool.getType().equals(STONE_PICKAXE));
        }
    }

    /**
     * Determines if a tool has the Silk Touch enchantment.
     *
     * @param stack the tool
     * @return true if the tool has Silk Touch, false otherwise
     */
    public boolean hasSilkTouch(ItemStack stack) {
        return stack.getEnchantments().getOrDefault(Enchantment.SILK_TOUCH, 0) > 0;
    }
}
