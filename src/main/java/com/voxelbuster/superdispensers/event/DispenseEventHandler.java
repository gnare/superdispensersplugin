package com.voxelbuster.superdispensers.event;

import com.voxelbuster.superdispensers.SuperDispensersPlugin;
import com.voxelbuster.superdispensers.util.GameCollections;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Jukebox;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Levelled;
import org.bukkit.block.data.type.Dispenser;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

import static org.bukkit.Material.*;

public class DispenseEventHandler implements Listener {
    private final Helpers h = new Helpers();

    private static final Logger log = SuperDispensersPlugin.getInstance().getLog();

    /**
     * Event handler for the dispensing of various items.
     *
     * @param event
     *     the Event triggered by the dispenser
     */
    @EventHandler
    public void onDispense(BlockDispenseEvent event) {
        Block dispenserBlock = event.getBlock();
        ItemStack stack = event.getItem();

        Material mat = stack.getType();
        int maxDurability = mat.getMaxDurability();

        FileConfiguration config = SuperDispensersPlugin.getInstance().getConfig();

        // Handle exclusions
        if (config.getStringList("disabledItems").contains(mat.getKey().toString())) {
            return;
        }

        for (String tag : config.getStringList("disabledTags")) {
            NamespacedKey key = NamespacedKey.fromString(tag);
            if (key == null) {
                 log.warning("Invalid tag in config: " + tag);
                continue;
            }

            if (
                Objects.requireNonNull(
                    Bukkit.getTag(Tag.REGISTRY_ITEMS, key, Material.class)
                ).getValues().contains(mat)
                    || Objects.requireNonNull(
                    Bukkit.getTag(Tag.REGISTRY_BLOCKS, key, Material.class)
                ).getValues().contains(mat)
            ) {
                return;
            }
        }

        // Do dispensing
        if (dispenserBlock.getState() instanceof org.bukkit.block.Dispenser dispenserState) {
            if (dispenserBlock.getBlockData() instanceof Dispenser dispenser) {
                BlockFace facing = dispenser.getFacing();

                org.bukkit.block.Dispenser blockState =
                    ((org.bukkit.block.Dispenser) dispenserBlock.getState());

                if (dispenserBlock.getRelative(facing).getType().equals(JUKEBOX) &&
                        dispenserBlock.getRelative(facing).getBlockPower() > 0) {
                    return; // ignore power received from jukebox
                }

                // SOLID BLOCKS
                if (
                    (mat.isBlock() && mat.isSolid() && mat.isOccluding())
                        || GameCollections.getPlacementOverrides().contains(mat)
                ) {
                    if (h.decrementInventoryByMaterial(blockState.getInventory(), stack) &&
                        h.attemptPlaceBlock(dispenserBlock, facing, mat)) {
                        event.setCancelled(true);
                    }
                }

                // CROPS/SEEDS
                if (GameCollections.getCropsMap().containsKey(mat)) {
                    if (h.attemptPlantSeeds(dispenserBlock, facing, mat)
                        && h.decrementInventoryByMaterial(blockState.getInventory(), stack)) {
                        event.setCancelled(true);
                    }
                }

                // SAPLINGS
                if (Tag.SAPLINGS.getValues().contains(mat)) {
                    if (h.decrementInventoryByMaterial(blockState.getInventory(), stack) &&
                        h.attemptPlantTree(dispenserBlock, facing, mat)) {
                        event.setCancelled(true);
                    }
                }

                // SWORDS
                if (Tag.ITEMS_SWORDS.getValues().contains(mat)) {
                    if (
                        !config.getBoolean("dropToolsOnLastDurability", true)
                        || (
                            stack.getItemMeta() instanceof org.bukkit.inventory.meta.Damageable d
                            && maxDurability - d.getDamage() > 1
                        )
                    ) {
                        event.setCancelled(true);
                    }
                    if (h.tryAttackEntity(dispenserBlock.getLocation().add(facing.getDirection()), stack)) {
                        h.damageItemStack(stack, dispenserState.getInventory());
                    }
                }

                // HOES
                if (Tag.ITEMS_HOES.getValues().contains(mat)) {
                    if (
                        !config.getBoolean("dropToolsOnLastDurability", true)
                            || (
                            stack.getItemMeta() instanceof org.bukkit.inventory.meta.Damageable d
                                && maxDurability - d.getDamage() > 1
                        )
                    ) {
                        event.setCancelled(true);
                    }
                    if (h.tryTillDirt(dispenserBlock, facing)) {
                        h.damageItemStack(stack, dispenserState.getInventory());
                    }
                }

                // SHOVELS
                if (Tag.ITEMS_SHOVELS.getValues().contains(mat)) {
                    if (
                        !config.getBoolean("dropToolsOnLastDurability", true)
                            || (
                            stack.getItemMeta() instanceof org.bukkit.inventory.meta.Damageable d
                                && maxDurability - d.getDamage() > 1
                        )
                    ) {
                        event.setCancelled(true);
                    }
                    if (h.tryPathDirt(dispenserBlock, facing)) {
                        h.damageItemStack(stack, dispenserState.getInventory());
                    }
                }

                // PICKAXES
                if (
                    Tag.ITEMS_PICKAXES.getValues().contains(mat)
                    && config.getBoolean("allowHarvestBlocksWithPickaxe")
                    && h.canHarvest(dispenserBlock.getRelative(facing).getType(), stack)
                ) {
                    if (
                        !config.getBoolean("dropToolsOnLastDurability", true)
                            || (
                            stack.getItemMeta() instanceof org.bukkit.inventory.meta.Damageable d
                                && maxDurability - d.getDamage() > 1
                        )
                    ) {
                        event.setCancelled(true);
                        Block b = dispenserBlock.getRelative(facing);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(
                            SuperDispensersPlugin.getInstance(),
                            () -> h.breakBlockWithDispenser(dispenserState, stack, dispenserBlock, b),
                            h.calcBreakTicks(b.getType(), stack)
                        );
                    }
                }

                // AXES
                if (Tag.ITEMS_AXES.getValues().contains(mat)) {
                    if (
                        !config.getBoolean("dropToolsOnLastDurability", true)
                            || (
                            stack.getItemMeta() instanceof org.bukkit.inventory.meta.Damageable d
                                && maxDurability - d.getDamage() > 1
                        )
                    ) {
                        event.setCancelled(true);
                    }
                    if (h.tryStripBlock(dispenserBlock, facing)) {
                        h.damageItemStack(stack, dispenserState.getInventory());
                    }
                }

                // BAMBOO
                if (mat.equals(BAMBOO)) {
                    if (h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)) {
                        if (
                            Tag.BAMBOO_PLANTABLE_ON.getValues()
                                .contains(dispenserBlock.getRelative(facing).getRelative(BlockFace.DOWN).getType())
                                && h.attemptPlaceBlock(dispenserBlock, facing, mat)
                        ) {
                            event.setCancelled(true);
                        }
                    }
                }

                // FLOWERS/GRASSES
                if (
                    Tag.FLOWERS.getValues().contains(mat)
                        || GameCollections.getGrasses().contains(mat)
                ) {
                    if (h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)) {
                        if (
                            Tag.DIRT.getValues()
                                .contains(dispenserBlock.getRelative(facing).getRelative(BlockFace.DOWN).getType())
                                && h.attemptPlaceBlock(dispenserBlock, facing, mat)
                        ) {
                            event.setCancelled(true);
                        }
                    }
                }

                // SEAGRASSES
                if (GameCollections.getSeagrasses().contains(mat)) {
                    if (
                        (
                            Tag.DIRT.getValues()
                                .contains(dispenserBlock.getRelative(facing).getRelative(BlockFace.DOWN).getType())
                                || Tag.SAND.getValues()
                                .contains(dispenserBlock.getRelative(facing).getRelative(BlockFace.DOWN).getType())
                        )
                            && dispenserBlock.getRelative(facing).getType().equals(WATER)
                            && h.attemptPlaceBlock(dispenserBlock, facing, mat)
                    ) {
                        if (h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)) {
                            event.setCancelled(true);
                        }
                    }
                }

                // MUSIC
                if (Tag.ITEMS_MUSIC_DISCS.getValues().contains(mat)) {
                    if (dispenserBlock.getRelative(facing).getState() instanceof Jukebox jukebox) {
                        if (h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)) {
                            Bukkit.getScheduler().scheduleSyncDelayedTask(
                                SuperDispensersPlugin.getInstance(),
                                () -> {
                                    jukebox.eject();
                                    Bukkit.getScheduler().scheduleSyncDelayedTask(
                                        SuperDispensersPlugin.getInstance(),
                                        () -> {
                                            jukebox.setRecord(stack);
                                            jukebox.setPlaying(mat);
                                            jukebox.update(true);
                                        },
                                        1L
                                    );
                                },
                                1L
                            );
                            event.setCancelled(true);
                        }
                    }
                }

                // BREEDING ANIMALS
                if (GameCollections.getBreedingItemsMap().containsKey(mat)) {
                    Breedable creature = h.getBreedableCreatures(dispenserBlock, facing, mat)
                        .findAny()
                        .orElse(null);
                    if (
                        creature != null
                            && h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)
                    ) {
                        event.setCancelled(true);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(
                            SuperDispensersPlugin.getInstance(),
                            () -> {
                                String cUuid = creature.getUniqueId().toString();

                                // ATTN: this may change between MC versions
                                Bukkit.getServer().dispatchCommand(
                                    Bukkit.getConsoleSender(),
                                    "data merge entity " + cUuid + " {InLove: 300}"
                                );

                                // Replace empty bucket
                                if (mat.toString().toUpperCase().contains("BUCKET")) {
                                    dispenserState.getInventory().addItem(new ItemStack(BUCKET, 1));
                                }

                                dispenserBlock.getWorld().spawnParticle(
                                    Particle.HEART,
                                    dispenserBlock.getRelative(facing).getLocation(),
                                    15,
                                    0.5d,
                                    0.5d,
                                    0.5d
                                );
                            },
                            1L
                        );
                    }
                }

                // EMPTY CAULDRONS
                if (GameCollections.getFluidCauldronsMap().containsKey(dispenserBlock.getRelative(facing).getType())) {
                    if (
                        stack.getType().equals(BUCKET)
                            && dispenserState.getInventory().firstEmpty() != -1
                            && h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)
                    ) {
                        Bukkit.getScheduler().scheduleSyncDelayedTask(
                            SuperDispensersPlugin.getInstance(),
                            () -> {
                                dispenserState.getInventory().addItem(
                                    new ItemStack(
                                        GameCollections.getFluidCauldronsMap()
                                            .get(dispenserBlock.getRelative(facing).getType()), 1
                                    )
                                );
                                dispenserBlock.getRelative(facing).setBlockData(Material.CAULDRON.createBlockData());
                            },
                            1L
                        );
                        event.setCancelled(true);
                    }
                }

                // FILL CAULDRON
                if (GameCollections.getFluidCauldronsMap().containsValue(mat)) {
                    if (
                        dispenserBlock.getRelative(facing).getType().equals(CAULDRON)
                            && dispenserState.getInventory().firstEmpty() != -1
                            && h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)
                    ) {
                        Bukkit.getScheduler().scheduleSyncDelayedTask(
                            SuperDispensersPlugin.getInstance(),
                            () -> {
                                dispenserState.getInventory().addItem(
                                    new ItemStack(BUCKET, 1)
                                );
                                BlockData data =
                                    GameCollections.getFluidCauldronsMap().keySet()
                                        .stream()
                                        .filter(fc -> GameCollections.getFluidCauldronsMap().get(fc).equals(mat))
                                        .findFirst()
                                        .orElseThrow()
                                        .createBlockData();
                                if (data instanceof Levelled levelled) {
                                    levelled.setLevel(levelled.getMaximumLevel());
                                    dispenserBlock.getRelative(facing).setBlockData(levelled);
                                } else {
                                    dispenserBlock.getRelative(facing).setBlockData(data);
                                }
                            },
                            1L
                        );
                        event.setCancelled(true);
                    }
                }

                // DYE SHEEP
                if (GameCollections.getDyesMap().containsKey(mat)) {
                    List<Entity> sheep = h.getNearbyEntities(dispenserBlock, facing, EntityType.SHEEP).toList();
                    if (!sheep.isEmpty()) {
                        if (h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)) {
                            event.setCancelled(true);
                            Bukkit.getScheduler().scheduleSyncDelayedTask(
                                SuperDispensersPlugin.getInstance(),
                                () -> {
                                    // Pick a random sheep and set its color
                                    ((org.bukkit.entity.Sheep) sheep.get((int) (Math.random() * sheep.size())))
                                        .setColor(GameCollections.getDyesMap().get(mat));
                                },
                                1L
                            );
                        }
                    }
                }

                if (mat.equals(SHEARS)) {
                    List<Entity> sheep = h.getNearbyEntities(dispenserBlock, facing, EntityType.SHEEP).toList();
                    if (!sheep.isEmpty()) {
                        event.setCancelled(true);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(
                            SuperDispensersPlugin.getInstance(),
                            () -> {
                                h.damageItemStack(stack, dispenserState.getInventory());

                                // Pick a random sheep and shear it
                                int index = (int) (Math.random() * sheep.size());

                                Sheep sheepEntity = (Sheep) sheep.get(index);
                                sheepEntity.setSheared(true);
                                DyeColor color = sheepEntity.getColor();
                                String colorWool = color.name() + "_WOOL";

                                dispenserBlock.getRelative(facing).getWorld()
                                        .dropItemNaturally(
                                                sheep.get(index).getLocation(),
                                                new ItemStack(
                                                        Material.valueOf(colorWool),
                                                        ThreadLocalRandom.current().nextInt(1, 3)
                                                )
                                        );
                            },
                            1L
                        );
                    }
                }

                // COMPOST
                if (dispenserBlock.getRelative(facing).getType().equals(COMPOSTER)) {
                    Levelled composter = (Levelled) dispenserBlock.getRelative(facing).getBlockData();
                    if (GameCollections.getCompostablesMap().containsKey(mat)) {
                        if (composter.getLevel() < 8) {
                            if (h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)) {
                                Bukkit.getScheduler().scheduleSyncDelayedTask(
                                    SuperDispensersPlugin.getInstance(),
                                    () -> {
                                        if (GameCollections.getCompostablesMap().get(mat) > Math.random()) {
                                            composter.setLevel(composter.getLevel() + 1);
                                            dispenserBlock.getRelative(facing).setBlockData(composter);
                                        }
                                    },
                                    1L
                                );
                                event.setCancelled(true);
                            }
                        }
                    }
                }

                // PLANT CHORUS
                if (mat.equals(CHORUS_FLOWER)) {
                    if (h.attemptPlantSeeds(dispenserBlock, facing, mat, END_STONE) &&
                            h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)
                    ) {
                        event.setCancelled(true);
                    }
                }

                // PLANT NETHER WART
                if (mat.equals(NETHER_WART)) {
                    if (h.attemptPlantSeeds(dispenserBlock, facing, mat, SOUL_SAND) &&
                            h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)
                    ) {
                        event.setCancelled(true);
                    }
                }

                // TODO automated dropper sorting?

                // lol
                if (mat.equals(DRAGON_EGG)) {
                    if (h.decrementInventoryByMaterial(dispenserState.getInventory(), stack)) {
                        Location blockLoc = dispenserBlock.getRelative(facing, 3).getLocation();
                        Bukkit.getScheduler().scheduleSyncDelayedTask(
                            SuperDispensersPlugin.getInstance(),
                            () -> {
                                EnderDragon e =
                                    (EnderDragon) dispenserBlock.getWorld().spawnEntity(
                                        blockLoc,
                                        EntityType.ENDER_DRAGON,
                                        true
                                    );
                                Bukkit.getScheduler().scheduleSyncDelayedTask(
                                    SuperDispensersPlugin.getInstance(),
                                    () -> e.setPhase(EnderDragon.Phase.CIRCLING),
                                    5L
                                );
                            },
                            2L
                        );
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    private static class Helpers extends DispenseEventHelpers {}
}
