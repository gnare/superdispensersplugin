package com.voxelbuster.superdispensers.util;

import lombok.Getter;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Map.entry;
import static org.bukkit.Material.*;

public class GameCollections {
    /**
     * Maps weapons to their damage values for
     * {@link com.voxelbuster.superdispensers.event.DispenseEventHelpers#tryAttackEntity(Location, ItemStack)}.
     */
    @Getter
    private static final Map<Material, Double> weaponDamageMap =
        Map.of(
            WOODEN_SWORD, 4.0,
            GOLDEN_SWORD, 4.0,
            STONE_SWORD, 5.0,
            IRON_SWORD, 6.0,
            DIAMOND_SWORD, 7.0,
            NETHERITE_SWORD, 8.0
        );
    @Getter
    private static final Set<EntityType> horseTypes =
        Set.of(
            EntityType.HORSE,
            EntityType.DONKEY
        );
    @Getter
    private static final Set<EntityType> farmAnimalsTypes =
        Set.of(
            EntityType.COW,
            EntityType.GOAT,
            EntityType.MUSHROOM_COW,
            EntityType.SHEEP
        );
    @Getter
    private static final Set<EntityType> pigType =
        Set.of(
            EntityType.PIG
        );
    @Getter
    private static final Set<EntityType> chickenType =
        Set.of(
            EntityType.CHICKEN
        );
    @Getter
    private static final Set<EntityType> wolfType =
        Set.of(
            EntityType.WOLF
        );
    @Getter
    private static final Set<EntityType> rabbitType =
        Set.of(
            EntityType.RABBIT
        );
    @Getter
    private static final Set<EntityType> foxType =
        Set.of(
            EntityType.RABBIT
        );
    @Getter
    private static final Set<EntityType> catTypes =
        Set.of(
            EntityType.CAT,
            EntityType.OCELOT
        );
    @Getter
    private static final Set<EntityType> llamaTypes =
        Set.of(
            EntityType.LLAMA,
            EntityType.TRADER_LLAMA
        );
    @Getter
    private static final Map<Material, Set<EntityType>> breedingItemsMap;

    /**
     * Maps seeds to their crop blocks.
     */
    @Getter
    private static final Map<Material, Material> cropsMap =
        Map.of(
            WHEAT_SEEDS, WHEAT,
            CARROT, CARROTS,
            POTATO, POTATOES,
            BEETROOT_SEEDS, BEETROOTS,
            MELON_SEEDS, MELON_STEM,
            PUMPKIN_SEEDS, PUMPKIN_STEM,
            TORCHFLOWER_SEEDS, TORCHFLOWER_CROP,
            NETHER_WART, NETHER_WART,
            CHORUS_FLOWER, CHORUS_FLOWER
        );
    /**
     * List of blocks that aren't included in the normal solid blocks that a dispenser should be able to place.
     */
    @Getter
    private static final List<Material> placementOverrides;
    /**
     * List of grass-like blocks.
     */
    @Getter
    private static final List<Material> grasses = List.of(
        SHORT_GRASS,
        TALL_GRASS,
        FERN,
        LARGE_FERN
    );
    /**
     * List of segrass-like blocks.
     */
    @Getter
    private static final List<Material> seagrasses = List.of(
        SEAGRASS,
        TALL_SEAGRASS,
        KELP,
        TUBE_CORAL,
        BRAIN_CORAL,
        BUBBLE_CORAL,
        FIRE_CORAL,
        HORN_CORAL,
        DEAD_BRAIN_CORAL,
        DEAD_BUBBLE_CORAL,
        DEAD_FIRE_CORAL,
        DEAD_HORN_CORAL,
        DEAD_TUBE_CORAL,
        TUBE_CORAL_FAN,
        BRAIN_CORAL_FAN,
        BUBBLE_CORAL_FAN,
        FIRE_CORAL_FAN,
        HORN_CORAL_FAN,
        DEAD_TUBE_CORAL_FAN,
        DEAD_BRAIN_CORAL_FAN,
        DEAD_BUBBLE_CORAL_FAN,
        DEAD_FIRE_CORAL_FAN,
        DEAD_HORN_CORAL_FAN
    );

    /**
     * Maps cauldrons to their fluid buckets.
     */
    @Getter
    private static final Map<Material, Material> fluidCauldronsMap = Map.of(
        LAVA_CAULDRON, LAVA_BUCKET,
        WATER_CAULDRON, WATER_BUCKET,
        POWDER_SNOW_CAULDRON, POWDER_SNOW_BUCKET
    );

    /**
     * Maps dyes to their colors.
     */
    @Getter
    private static final Map<Material, DyeColor> dyesMap = Map.ofEntries(
            entry(WHITE_DYE, DyeColor.WHITE),
            entry(ORANGE_DYE, DyeColor.ORANGE),
            entry(MAGENTA_DYE, DyeColor.MAGENTA),
            entry(LIGHT_BLUE_DYE, DyeColor.LIGHT_BLUE),
            entry(YELLOW_DYE, DyeColor.YELLOW),
            entry(LIME_DYE, DyeColor.LIME),
            entry(PINK_DYE, DyeColor.PINK),
            entry(GRAY_DYE, DyeColor.GRAY),
            entry(LIGHT_GRAY_DYE, DyeColor.LIGHT_GRAY),
            entry(CYAN_DYE, DyeColor.CYAN),
            entry(PURPLE_DYE, DyeColor.PURPLE),
            entry(BLUE_DYE, DyeColor.BLUE),
            entry(BROWN_DYE, DyeColor.BROWN),
            entry(GREEN_DYE, DyeColor.GREEN),
            entry(RED_DYE, DyeColor.RED),
            entry(BLACK_DYE, DyeColor.BLACK)
    );

    /**
     * Maps compostable items to the chance they turn into compost.
     */
    @Getter
    private static final Map<Material, Float> compostablesMap = Map.ofEntries(
        entry(CAKE, 1f),
        entry(PUMPKIN_PIE, 1f),

        entry(BAKED_POTATO, 0.85f),
        entry(BREAD, 0.85f),
        entry(COOKIE, 0.85f),
        entry(FLOWERING_AZALEA, 0.85f),
        entry(HAY_BLOCK, 0.85f),
        entry(RED_MUSHROOM_BLOCK, 0.85f),
        entry(BROWN_MUSHROOM_BLOCK, 0.85f),
        entry(NETHER_WART_BLOCK, 0.85f),
        entry(PITCHER_PLANT, 0.85f),
        entry(TORCHFLOWER, 0.85f),
        entry(WARPED_WART_BLOCK, 0.85f),

        entry(APPLE, 0.65f),
        entry(AZALEA, 0.65f),
        entry(BEETROOT, 0.65f),
        entry(BIG_DRIPLEAF, 0.65f),
        entry(CARROT, 0.65f),
        entry(COCOA_BEANS, 0.65f),

        entry(ALLIUM, 0.65f),
        entry(AZURE_BLUET, 0.65f),
        entry(BLUE_ORCHID, 0.65f),
        entry(CORNFLOWER, 0.65f),
        entry(DANDELION, 0.65f),
        entry(LILY_OF_THE_VALLEY, 0.65f),
        entry(OXEYE_DAISY, 0.65f),
        entry(POPPY, 0.65f),
        entry(ORANGE_TULIP, 0.65f),
        entry(PINK_TULIP, 0.65f),
        entry(RED_TULIP, 0.65f),
        entry(WHITE_TULIP, 0.65f),
        entry(WITHER_ROSE, 0.65f),
        entry(LILAC, 0.65f),
        entry(PEONY, 0.65f),
        entry(ROSE_BUSH, 0.65f),
        entry(SUNFLOWER, 0.65f),

        entry(WARPED_FUNGUS, 0.65f),
        entry(CRIMSON_FUNGUS, 0.65f),
        entry(LILY_PAD, 0.65f),
        entry(MELON, 0.65f),
        entry(MOSS_BLOCK, 0.65f),
        entry(BROWN_MUSHROOM, 0.65f),
        entry(RED_MUSHROOM, 0.65f),
        entry(MUSHROOM_STEM, 0.65f),
        entry(NETHER_WART, 0.65f),
        entry(POTATO, 0.65f),
        entry(PUMPKIN, 0.65f),
        entry(CARVED_PUMPKIN, 0.65f),
        entry(WARPED_ROOTS, 0.65f),
        entry(CRIMSON_ROOTS, 0.65f),
        entry(SEA_PICKLE, 0.65f),
        entry(SHROOMLIGHT, 0.65f),
        entry(SPORE_BLOSSOM, 0.65f),
        entry(WHEAT, 0.65f),

        entry(CACTUS, 0.5f),
        entry(DRIED_KELP_BLOCK, 0.5f),
        entry(FLOWERING_AZALEA_LEAVES, 0.5f),
        entry(GLOW_LICHEN, 0.5f),
        entry(MELON_SLICE, 0.5f),
        entry(NETHER_SPROUTS, 0.5f),
        entry(SUGAR_CANE, 0.5f),
        entry(TALL_GRASS, 0.5f),
        entry(TWISTING_VINES, 0.5f),
        entry(VINE, 0.5f),
        entry(WEEPING_VINES, 0.5f),

        entry(BEETROOT_SEEDS, 0.3f),
        entry(DRIED_KELP, 0.3f),
        entry(GLOW_BERRIES, 0.3f),
        entry(HANGING_ROOTS, 0.3f),
        entry(MANGROVE_ROOTS, 0.3f),
        entry(KELP, 0.3f),
        entry(SHORT_GRASS, 0.3f),

        entry(OAK_LEAVES, 0.3f),
        entry(BIRCH_LEAVES, 0.3f),
        entry(DARK_OAK_LEAVES, 0.3f),
        entry(JUNGLE_LEAVES, 0.3f),
        entry(ACACIA_LEAVES, 0.3f),
        entry(SPRUCE_LEAVES, 0.3f),
        entry(MANGROVE_LEAVES, 0.3f),
        entry(CHERRY_LEAVES, 0.3f),

        entry(MELON_SEEDS, 0.3f),
        entry(MOSS_CARPET, 0.3f),
        entry(PINK_PETALS, 0.3f),
        entry(PITCHER_POD, 0.3f),
        entry(PUMPKIN_SEEDS, 0.3f),

        entry(OAK_SAPLING, 0.3f),
        entry(DARK_OAK_SAPLING, 0.3f),
        entry(BIRCH_SAPLING, 0.3f),
        entry(ACACIA_SAPLING, 0.3f),
        entry(JUNGLE_SAPLING, 0.3f),
        entry(SPRUCE_SAPLING, 0.3f),
        entry(CHERRY_SAPLING, 0.3f),
        entry(MANGROVE_PROPAGULE, 0.3f),

        entry(SEAGRASS, 0.3f),
        entry(SMALL_DRIPLEAF, 0.3f),
        entry(SWEET_BERRIES, 0.3f),
        entry(TORCHFLOWER_SEEDS, 0.3f),
        entry(WHEAT_SEEDS, 0.3f)
    );

    /**
     * Maps blocks to their stripped variants.
     */
    @Getter
    private static final Map<Material, Material> strippablesMap = Map.<Material, Material>ofEntries(
        // logs
        entry(ACACIA_LOG, STRIPPED_ACACIA_LOG),
        entry(OAK_LOG, STRIPPED_OAK_LOG),
        entry(BIRCH_LOG, STRIPPED_BIRCH_LOG),
        entry(SPRUCE_LOG, STRIPPED_SPRUCE_LOG),
        entry(DARK_OAK_LOG, STRIPPED_DARK_OAK_LOG),
        entry(JUNGLE_LOG, STRIPPED_JUNGLE_LOG),
        entry(MANGROVE_LOG, STRIPPED_MANGROVE_LOG),
        entry(CHERRY_LOG, STRIPPED_CHERRY_LOG),
        entry(CRIMSON_STEM, STRIPPED_CRIMSON_STEM),
        entry(WARPED_STEM, STRIPPED_WARPED_STEM),
        entry(CRIMSON_HYPHAE, STRIPPED_CRIMSON_HYPHAE),

        // wood
        entry(ACACIA_WOOD, STRIPPED_ACACIA_WOOD),
        entry(OAK_WOOD, STRIPPED_OAK_WOOD),
        entry(BIRCH_WOOD, STRIPPED_BIRCH_WOOD),
        entry(SPRUCE_WOOD, STRIPPED_SPRUCE_WOOD),
        entry(DARK_OAK_WOOD, STRIPPED_DARK_OAK_WOOD),
        entry(JUNGLE_WOOD, STRIPPED_JUNGLE_WOOD),
        entry(MANGROVE_WOOD, STRIPPED_MANGROVE_WOOD),
        entry(CHERRY_WOOD, STRIPPED_CHERRY_WOOD),
        entry(BAMBOO_BLOCK, STRIPPED_BAMBOO_BLOCK),

        // dewax
        entry(WAXED_COPPER_BLOCK, COPPER_BLOCK),
        entry(WAXED_EXPOSED_COPPER, EXPOSED_COPPER),
        entry(WAXED_WEATHERED_COPPER, WEATHERED_COPPER),
        entry(WAXED_OXIDIZED_COPPER, OXIDIZED_COPPER),
        entry(WAXED_CHISELED_COPPER, CHISELED_COPPER),
        entry(WAXED_EXPOSED_CHISELED_COPPER, EXPOSED_CHISELED_COPPER),
        entry(WAXED_WEATHERED_CHISELED_COPPER, WEATHERED_CHISELED_COPPER),
        entry(WAXED_OXIDIZED_CHISELED_COPPER, OXIDIZED_CHISELED_COPPER),
        entry(WAXED_CUT_COPPER, CUT_COPPER),
        entry(WAXED_EXPOSED_CUT_COPPER, EXPOSED_CUT_COPPER),
        entry(WAXED_WEATHERED_CUT_COPPER, WEATHERED_CUT_COPPER),
        entry(WAXED_OXIDIZED_CUT_COPPER, OXIDIZED_CUT_COPPER),
        entry(WAXED_CUT_COPPER_STAIRS, CUT_COPPER_STAIRS),
        entry(WAXED_EXPOSED_CUT_COPPER_STAIRS, EXPOSED_CUT_COPPER_STAIRS),
        entry(WAXED_WEATHERED_CUT_COPPER_STAIRS, WEATHERED_CUT_COPPER_STAIRS),
        entry(WAXED_OXIDIZED_CUT_COPPER_STAIRS, OXIDIZED_CUT_COPPER_STAIRS),
        entry(WAXED_CUT_COPPER_SLAB, CUT_COPPER_SLAB),
        entry(WAXED_EXPOSED_CUT_COPPER_SLAB, EXPOSED_CUT_COPPER_SLAB),
        entry(WAXED_WEATHERED_CUT_COPPER_SLAB, WEATHERED_CUT_COPPER_SLAB),
        entry(WAXED_OXIDIZED_CUT_COPPER_SLAB, OXIDIZED_CUT_COPPER_SLAB),
        entry(WAXED_COPPER_DOOR, COPPER_DOOR),
        entry(WAXED_EXPOSED_COPPER_DOOR, EXPOSED_COPPER_DOOR),
        entry(WAXED_WEATHERED_COPPER_DOOR, WEATHERED_COPPER_DOOR),
        entry(WAXED_OXIDIZED_COPPER_DOOR, OXIDIZED_COPPER_DOOR),
        entry(WAXED_COPPER_TRAPDOOR, COPPER_TRAPDOOR),
        entry(WAXED_EXPOSED_COPPER_TRAPDOOR, EXPOSED_COPPER_TRAPDOOR),
        entry(WAXED_WEATHERED_COPPER_TRAPDOOR, WEATHERED_COPPER_TRAPDOOR),
        entry(WAXED_OXIDIZED_COPPER_TRAPDOOR, OXIDIZED_COPPER_TRAPDOOR),
        entry(WAXED_COPPER_GRATE, COPPER_GRATE),
        entry(WAXED_EXPOSED_COPPER_GRATE, EXPOSED_COPPER_GRATE),
        entry(WAXED_WEATHERED_COPPER_GRATE, WEATHERED_COPPER_GRATE),
        entry(WAXED_OXIDIZED_COPPER_GRATE, OXIDIZED_COPPER_GRATE),
        entry(WAXED_COPPER_BULB, COPPER_BULB),
        entry(WAXED_EXPOSED_COPPER_BULB, EXPOSED_COPPER_BULB),
        entry(WAXED_WEATHERED_COPPER_BULB, WEATHERED_COPPER_BULB),
        entry(WAXED_OXIDIZED_COPPER_BULB, OXIDIZED_COPPER_BULB),

        // scraping
        entry(OXIDIZED_COPPER, COPPER_BLOCK),
        entry(OXIDIZED_CHISELED_COPPER, CHISELED_COPPER),
        entry(OXIDIZED_CUT_COPPER, CUT_COPPER),
        entry(OXIDIZED_CUT_COPPER_STAIRS, CUT_COPPER_STAIRS),
        entry(OXIDIZED_CUT_COPPER_SLAB, CUT_COPPER_SLAB),
        entry(OXIDIZED_COPPER_DOOR, COPPER_DOOR),
        entry(OXIDIZED_COPPER_TRAPDOOR, COPPER_TRAPDOOR),
        entry(OXIDIZED_COPPER_GRATE, COPPER_GRATE),
        entry(OXIDIZED_COPPER_BULB, COPPER_BULB),

        entry(WEATHERED_COPPER, COPPER_BLOCK),
        entry(WEATHERED_CHISELED_COPPER, CHISELED_COPPER),
        entry(WEATHERED_CUT_COPPER, CUT_COPPER),
        entry(WEATHERED_CUT_COPPER_STAIRS, CUT_COPPER_STAIRS),
        entry(WEATHERED_CUT_COPPER_SLAB, CUT_COPPER_SLAB),
        entry(WEATHERED_COPPER_DOOR, COPPER_DOOR),
        entry(WEATHERED_COPPER_TRAPDOOR, COPPER_TRAPDOOR),
        entry(WEATHERED_COPPER_GRATE, COPPER_GRATE),
        entry(WEATHERED_COPPER_BULB, COPPER_BULB),

        entry(EXPOSED_COPPER, COPPER_BLOCK),
        entry(EXPOSED_CHISELED_COPPER, CHISELED_COPPER),
        entry(EXPOSED_CUT_COPPER, CUT_COPPER),
        entry(EXPOSED_CUT_COPPER_STAIRS, CUT_COPPER_STAIRS),
        entry(EXPOSED_CUT_COPPER_SLAB, CUT_COPPER_SLAB),
        entry(EXPOSED_COPPER_DOOR, COPPER_DOOR),
        entry(EXPOSED_COPPER_TRAPDOOR, COPPER_TRAPDOOR),
        entry(EXPOSED_COPPER_GRATE, COPPER_GRATE),
        entry(EXPOSED_COPPER_BULB, COPPER_BULB)
    );

    @Getter
    private static final List<EntityType> undead = List.of(
        EntityType.ZOMBIE,
        EntityType.ZOMBIE_VILLAGER,
        EntityType.ZOMBIFIED_PIGLIN,
        EntityType.ZOGLIN,
        EntityType.SKELETON,
        EntityType.WITHER_SKELETON,
        EntityType.SKELETON_HORSE,
        EntityType.ZOMBIE_HORSE,
        EntityType.STRAY,
        EntityType.HUSK,
        EntityType.PHANTOM,
        EntityType.DROWNED
    );

    @Getter
    private static final List<EntityType> arthropods = List.of(
        EntityType.SPIDER,
        EntityType.CAVE_SPIDER,
        EntityType.SILVERFISH,
        EntityType.BEE,
        EntityType.ENDERMITE
    );

    static {
        // Block placement overrides
        ArrayList<Material> placementOverridesTemp = new ArrayList<>(List.of(
            ANVIL,
            CHIPPED_ANVIL,
            DAMAGED_ANVIL,
            REDSTONE_BLOCK,
            OBSERVER,
            COMPOSTER,
            CAULDRON,
            HOPPER,
            SNIFFER_EGG,
            TURTLE_EGG
        ));
        placementOverridesTemp.addAll(Tag.IMPERMEABLE.getValues());
        placementOverrides = placementOverridesTemp;

        // Breeding items
        breedingItemsMap = new Hashtable<>();
        Tag.FLOWERS.getValues()
            .forEach(material -> breedingItemsMap.put(material, Set.of(EntityType.BEE)));

        breedingItemsMap.putAll(
            Map.ofEntries(
                entry(GOLDEN_APPLE, horseTypes),
                entry(ENCHANTED_GOLDEN_APPLE, horseTypes),
                entry(WHEAT, farmAnimalsTypes),
                entry(CARROT, Set.of(EntityType.PIG, EntityType.RABBIT)),
                entry(POTATO, pigType),
                entry(BEETROOT, pigType),
                entry(WHEAT_SEEDS, chickenType),
                entry(PUMPKIN_SEEDS, chickenType),
                entry(MELON_SEEDS, chickenType),
                entry(BEETROOT_SEEDS, chickenType),
                entry(TORCHFLOWER_SEEDS, Set.of(EntityType.CHICKEN, EntityType.SNIFFER)),
                entry(PITCHER_POD, chickenType),
                entry(BEEF, wolfType),
                entry(CHICKEN, wolfType),
                entry(PORKCHOP, wolfType),
                entry(MUTTON, wolfType),
                entry(RABBIT, wolfType),
                entry(ROTTEN_FLESH, wolfType),
                entry(COOKED_BEEF, wolfType),
                entry(COOKED_CHICKEN, wolfType),
                entry(COOKED_PORKCHOP, wolfType),
                entry(COOKED_MUTTON, wolfType),
                entry(COOKED_RABBIT, wolfType),
                entry(COD, catTypes),
                entry(SALMON, catTypes),
                entry(TROPICAL_FISH_BUCKET, Set.of(EntityType.AXOLOTL)),
                entry(HAY_BLOCK, llamaTypes),
                entry(DANDELION, rabbitType),
                entry(GOLDEN_CARROT, rabbitType),
                entry(SEAGRASS, Set.of(EntityType.TURTLE)),
                entry(BAMBOO, Set.of(EntityType.PANDA)),
                entry(SWEET_BERRIES, foxType),
                entry(GLOW_BERRIES, foxType),
                entry(WARPED_FUNGUS, Set.of(EntityType.STRIDER)),
                entry(SLIME_BALL, Set.of(EntityType.FROG)),
                entry(CACTUS, Set.of(EntityType.CAMEL))
            )
        );
    }
}
