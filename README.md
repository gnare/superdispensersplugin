# Super Dispensers

A plugin to enhance the functionality of dispensers.

### Plugin Showcase:
https://cdn.voxelbuster.com/9eacf6f0114959dff288b7107331508dd7390d6c.mp4

### Important Troubleshooting Note:
Before reporting any issues, please verify you are on a supported version, and a
supported Spigot distribution; meaning any of the following:
 - Official Spigot
 - Official CraftBukkit
 - Paper

If Minecraft was recently updated, and you are having issues with the plugin,
please try reproducing your issue on a server without other plugins. The 
Spigot API changes very little from version to version so it is more
likely you have a plugin conflict.

With that in mind, please direct all bug reports or feature requests here:
https://gitlab.com/gnare/superdispensersplugin/-/issues

### Features:
- Most solid, full blocks can be placed by dispensers
- Some additional blocks can be placed, including:
   - Glass blocks
   - Sand and gravel
   - Hatchable eggs
- Music discs can be inserted into jukeboxes
   - Does nothing if disc is already inserted
   - Hoppers already do this too
- Trees, flowers, grasses, and bamboo can all be planted on dirt
  - Seagrass can be planted if there is water in front of the dispenser
- All crops can be planted
  - Requires tilled soil
- Dirt can be tilled with a hoe
- Logs can be stripped with an axe
- Living entities can be attacked with a sword
- All used tools will use durability
  - Uses unbreaking enchants properly
  - Also correctly uses Silk Touch, Smite, BoA, and Sharpness
- Can spawn ender dragons
- Can plant nether wart
- Can plant chorus plants
- Can dye sheep
- Can shear sheep
- Can add items to composters

### Configuration:
```yaml
# A list of item IDs you would like to not change the default behavior for, e.g.:
disabledItems: ['minecraft:stone']

# A list of Minecraft item tags you would like to not change the default behavior for, e.g.:
disabledTags: ['minecraft:flowers']

# Whether to have the dispenser drop the tool when it reaches its last durability (true/false)
dropToolsOnLastDurability: true

# Whether to allow dispensers to harvest blocks with tools (true/false)
allowHarvestBlocksWithPickaxe: false
```

That's it, the config is very simple.
There's no commands and no permissions.


This plugin should practically work on any variant of Spigot,
since it has no external dependencies.
See the code at https://gitlab.com/gnare/superdispensersplugin.